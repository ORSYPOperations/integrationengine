package com.orsyp.integration.engine.testing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.orsyp.integration.engine.messages.FailUprocEvent;
import com.orsyp.integration.engine.messages.LogUprocEvent;
import com.orsyp.integration.engine.messages.ServerEvent;
import com.orsyp.integration.engine.messages.StartUprocEvent;
import com.orsyp.integration.engine.messages.StopUprocEvent;
import com.orsyp.integration.engine.server.IDataProvider;

public class TextFileDataProvider implements IDataProvider {

	@Override
	public List<ServerEvent> getActivity() {
		List<ServerEvent> list = new ArrayList<ServerEvent>();
		try {
			Scanner scanIn = new Scanner(new File("C:/java/eclipse indigo/wsp/IntegrationEngine/config/testmsg.txt"));
			
			while (scanIn.hasNext()) {
				String text = scanIn.nextLine();	 
			    
			    String s[] = text.split(" ");
		    	ServerEvent ev = null;
		    	if (s[0].equalsIgnoreCase("START")) {
		    		ev = new StartUprocEvent();
		    		ev.serverJobId = s[1];
		    		ev.serverLaunchId = s[2];
		    		ev.message = s[3];
		    	}
		    	else
	    		if (s[0].equalsIgnoreCase("STOP")) {
	    			ev = new StopUprocEvent();
	    			ev.serverJobId = s[1];
		    		ev.serverLaunchId = s[2];
		    		ev.message = s[3];
		    	}
	    		else
	    		if (s[0].equalsIgnoreCase("FAIL")) {
	    			ev = new FailUprocEvent();
	    			ev.serverJobId = s[1];
		    		ev.serverLaunchId = s[2];
		    		ev.message = s[3];
		    	}		    	
	    		else
	    		if (s[0].equalsIgnoreCase("LOG")) {
	    			ev = new LogUprocEvent();
	    			ev.serverJobId = s[1];
		    		ev.serverLaunchId = s[2];
		    		ev.message = s[3];
		    	}
		    	
		    	if (ev!=null)
		    		list.add(ev);
			}
			scanIn.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
		return list;
	}

	@Override
	public void startJob(String jobId) {
		// TODO Auto-generated method stub
		System.out.println("DATA PROVIDER --- START server job:"  + jobId);
	}

	@Override
	public void stopJob(String jobId, String launchId) {
		// TODO Auto-generated method stub
		System.out.println("DATA PROVIDER --- STOP server job:"  + jobId + " Launch:" + launchId);
	}

}
