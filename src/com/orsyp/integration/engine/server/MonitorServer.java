package com.orsyp.integration.engine.server;

import java.util.List;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.orsyp.integration.engine.du.DuApi;
import com.orsyp.integration.engine.du.IDuCommunication;
import com.orsyp.integration.engine.messages.FailUprocEvent;
import com.orsyp.integration.engine.messages.LogUprocEvent;
import com.orsyp.integration.engine.messages.RegistrationEvent;
import com.orsyp.integration.engine.messages.ServerEvent;
import com.orsyp.integration.engine.messages.StartUprocEvent;
import com.orsyp.integration.engine.messages.StopUprocEvent;
import com.orsyp.integration.engine.util.CustomProperties;
import com.orsyp.integration.engine.util.Log;

public class MonitorServer {

	public static void main(String[] args) {
		
		int port = 32000;
		int pollingSeconds = 60;
		IDataProvider dataProvider = null;
		//default api
		IDuCommunication duComm = new DuApi();
		String clientScript = "";
		
		if (args.length!=1) {
			Log.log("ERROR! Pass config file as startup parameter");
			System.exit(1);
		}
		
		String configFile = args[0];
		CustomProperties config = null;
		
		try {
			config = new CustomProperties();
			config.load(configFile);			
		} catch (Exception e) {
			Log.log("ERROR reading config file: " + args[0]);
			System.exit(1);
		}			
		
		try {
			port = Integer.parseInt(config.getProperty("port"));
			pollingSeconds = Integer.parseInt(config.getProperty("pollingSeconds"));
			dataProvider = (IDataProvider) (Class.forName(config.getProperty("dataProviderClass"))).newInstance();
			if (config.getProperty("duCommunicationClass")!=null)
				duComm = (IDuCommunication) (Class.forName(config.getProperty("duCommunicationClass"))).newInstance();			
			duComm.configure(config);			
			
			int idx=1;
			while (config.getProperty("clientScript"+idx)!=null ) {
				if (clientScript.length()>0)
					clientScript += "\n";
				clientScript += config.getProperty("clientScript"+idx);
				idx++;
			}
			
		} catch (Exception e) {	
			Log.log("ERROR configuring server");
			e.printStackTrace();
			System.exit(1);
		}
		
		try {
			new MonitorServer(port,pollingSeconds,dataProvider,duComm,clientScript);
		} catch (Exception e) {	
			Log.log("Server ERROR");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	
	private Server server;
	private ProcessTracker processTracker = new ProcessTracker(); 
	
	public MonitorServer(int port, int pollingSeconds, 
						final IDataProvider dataProvider, final IDuCommunication duComm,
						final String clientScript) 
						throws Exception {
	
		server = new Server() {
				            protected Connection newConnection () {
				            	Log.log("Client Connected");
				                return super.newConnection();
					        }
						};
		
		ClassRegistration.register(server);
        
        server.addListener(new Listener() {
				            		public void received (Connection c, Object object) {
				            			if (object instanceof RegistrationEvent) {
				            				RegistrationEvent e = (RegistrationEvent) object;
				            				Log.log("Register client: UPR=" + e.clientUProcName + " - LAUNCH=" + e.clientLaunchId + " - ID=" + e.serverJobId + "/" +  e.serverLaunchId);
				            				
				            				LogUprocEvent evt = new LogUprocEvent();
				            				evt.message = "Server says: Welcome!";
				            				c.sendTCP(evt);
				            				
				            				MonitoredProcess mp = processTracker.getPendingUProcProcess(e.serverJobId,e.serverLaunchId);
				            				//server job already running - uproc started by server event
				            				if (mp!=null) {
					            				mp.clientLaunchId = e.clientLaunchId;
					            				mp.clientUProcName = e.clientUProcName;
					            				mp.connection = c;
					            				processTracker.logStatus("Client registered");
					            				
					            				while (mp.messageQueue.size()>0) {
					            					ServerEvent ev = mp.messageQueue.remove(0);
						            				try {
														processEvent(ev, duComm, clientScript);
													} catch (Exception ex) {
														ex.printStackTrace();
													}
					            				}
				            				}
				            				else {
				            					//uproc started by user, start server job
				            					mp = processTracker.newUProcConnection(e.clientUProcName, e.clientLaunchId, e.serverJobId);
				            					mp.connection = c;
				            					Log.log("Launch job on target system: UPR=" + e.clientUProcName + " - LAUNCH=" + e.clientLaunchId + " - ID=" + e.serverJobId + "/" + e.serverLaunchId);
				            					dataProvider.startJob(e.serverJobId);
				            				}
				            			}
				            		}
				            			
				            		public void disconnected (Connection c) {
				            			Log.log("Client Disconnected");
				            			MonitoredProcess mp = processTracker.getMatchingProcess(c);				            							            			
				            			
				            			if (mp!=null) {
				            				processTracker.remove(mp);
			            					dataProvider.stopJob(mp.serverProcessId, mp.serverLaunchId);
			            					Log.log("Stop job on target system: UPR=" + mp.clientUProcName + " - LAUNCH=" + mp.clientLaunchId + " - ID=" + mp.serverProcessId +"/" +mp.serverLaunchId);
			            				}
				            			else
				            				Log.log("Connection not found");
				            		}
				            		
				        		});
        
        server.bind(port);
        server.start();
        Log.log("Server started");
        
        while (true) {
        	List<ServerEvent> l = dataProvider.getActivity();
        	for (ServerEvent ev : l) 
        		processEvent(ev,duComm,clientScript);        	
        	Thread.sleep(1000 * pollingSeconds);
        }
	}
	
	
	
	private void processEvent(ServerEvent ev, IDuCommunication duComm, String clientScript) throws Exception {
		if (ev instanceof StartUprocEvent) {
			String upr = processTracker.getUProcName(ev.serverJobId);
			MonitoredProcess mp = processTracker.getMatchingUProcProcess(ev.serverJobId, ev.serverLaunchId);
			if (mp==null) { 
				mp = processTracker.getPendingServerProcess(ev.serverJobId);
				if (mp==null) { 
    				processTracker.newServerConnection(ev.serverJobId, ev.serverLaunchId, upr);
    				Log.log("Start UProc: UPR=" + upr + " - LAUNCH=? - ID=" + ev.serverJobId +"/" +ev.serverLaunchId);
        			if (!duComm.existsUProc(upr))
        				duComm.createUProc(upr, ev.serverJobId, clientScript);
        			duComm.launchUProc(upr, ev.serverLaunchId);
				}
				else {
					mp.serverLaunchId = ev.serverLaunchId;
					Log.log("Server started matching process, uproc already running: UPR=" + upr + " - LAUNCH="+ ev.clientLaunchId + " - ID=" + ev.serverJobId +"/" +ev.serverLaunchId);
				}
			}
			else {
				Log.log("Server process already monitored: UPR=" + upr + " - ID=" + ev.serverJobId +"/" +ev.serverLaunchId);
			}
		}
		else
		if (ev instanceof StopUprocEvent || ev instanceof FailUprocEvent) {
			MonitoredProcess mp = processTracker.getMatchingUProcProcess(ev.serverJobId, ev.serverLaunchId);
			if (mp!=null) {    					
				Log.log("STOP/FAIL '"+ev.message+"' on  ID=" + ev.serverJobId +"/" +ev.serverLaunchId);
				if (mp.connection!=null) {
					mp.connection.sendTCP(ev);
					processTracker.remove(mp);
				}
				else {
					mp.messageQueue.add(ev);
					Log.log("--->>> Queued");
				}
			}
			else
				if (ev instanceof StopUprocEvent) 
					System.out.println("STOP - No connection for: " + ev.serverJobId +"/" +ev.serverLaunchId);
				else
					System.out.println("FAIL - No connection for: " + ev.serverJobId +"/" +ev.serverLaunchId);
		}
		else
		if (ev instanceof LogUprocEvent) {
			MonitoredProcess mp = processTracker.getMatchingUProcProcess(ev.serverJobId, ev.serverLaunchId);
			if (mp!=null) {
				if (mp.connection!=null) {
					mp.connection.sendTCP(ev);
					Log.log("LOG '"+ev.message+"' on  ID=" + ev.serverJobId +"/" +ev.serverLaunchId);
				}
				else {
					mp.messageQueue.add(ev);
					Log.log("--->>> Queued");
				}
			}
			else
				System.out.println("LOG - No connection for: " + ev.serverJobId+"/" +ev.serverLaunchId);
		} 		
	}

}
