package com.orsyp.integration.engine.server;

import java.util.Vector;

import com.esotericsoftware.kryonet.Connection;
import com.orsyp.integration.engine.messages.ServerEvent;

public class MonitoredProcess {
	
	//target system job name
	public String serverProcessId;
	//target system job launch id
	public String serverLaunchId;
	
	//uproc name
	public String clientUProcName;
	//launch id
	public String clientLaunchId;
	
	//connection object
	public Connection connection;
	
	public Vector<ServerEvent> messageQueue = new Vector<ServerEvent>(); 
	
}
