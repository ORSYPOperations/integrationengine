package com.orsyp.integration.engine.server;

import java.util.List;

import com.orsyp.integration.engine.messages.ServerEvent;

public interface IDataProvider {
	
	public List<ServerEvent> getActivity();
	
	public void startJob(String jobId);
	
	public void stopJob(String jobId, String launchId);

}
