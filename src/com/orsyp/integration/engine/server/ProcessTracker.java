package com.orsyp.integration.engine.server;

import java.util.Vector;

import com.esotericsoftware.kryonet.Connection;

public class ProcessTracker {
	
	//list of all active processes
	private Vector<MonitoredProcess> processes = new Vector<MonitoredProcess>();	
	
	/**
	 * Get matching uproc name for server process
	 * @param serverProcessId
	 * @return
	 */
	public String getUProcName(String serverProcessId) {
		//TODO match uproc name/server process name 
		return serverProcessId;
	}	
	
	/**
	 * Add a new process in the list. Started by the server
	 * 
	 * @param serverProcessId
	 * @param serverLaunchId
	 * @param uprocName
	 * @return
	 */
	public MonitoredProcess newServerConnection(String serverProcessId, String serverLaunchId, String uprocName) {
		logStatus("Adding new server connection " + serverProcessId);
		for (MonitoredProcess pr: processes) 
			if (serverLaunchId.equals(pr.serverLaunchId) && serverProcessId.equals(pr.serverProcessId))
				return null;

		MonitoredProcess pr = new MonitoredProcess();
		pr.serverLaunchId = serverLaunchId;
		pr.serverProcessId = serverProcessId;
		pr.clientUProcName = uprocName;
		processes.add(pr);
		logStatus("Added new server connection " + serverProcessId);
		return pr;
	}

	/**
	 * Add a new process in the list. Started by an uproc
	 * @param clientUProcName
	 * @param clientLaunchId
	 * @param serverJobId
	 * @return
	 */
	public MonitoredProcess newUProcConnection(String clientUProcName, String clientLaunchId, String serverJobId) {
		logStatus("adding new uproc connection " + clientUProcName);
		for (MonitoredProcess pr: processes) 
			if (clientLaunchId.equals(pr.clientLaunchId) && clientUProcName.equals(pr.clientUProcName))
				return null;

		MonitoredProcess pr = new MonitoredProcess();
		pr.clientLaunchId = clientLaunchId;
		pr.clientUProcName = clientUProcName;
		pr.serverProcessId = serverJobId;
		processes.add(pr);
		logStatus("Added new uproc connection " + clientUProcName);
		return pr;
	}
	
	/**
	 * Get a process that matches uproc name and uproc launch id
	 * 
	 * @param clientUProcName
	 * @param clientLaunchId
	 * @return
	 */
	public MonitoredProcess getMatchingServerProcess(String clientUProcName, String clientLaunchId) {
		logStatus("getMatchingServerProcess");
		for (MonitoredProcess pr: processes) 
			if (clientLaunchId.equals(pr.clientLaunchId) && clientUProcName.equals(pr.clientUProcName))
				return pr;
		return null;
	}
	
	/**
	 * Get a process that matches server job id and launch id
	 * @param serverProcessId
	 * @param serverLaunchId
	 * @return
	 */
	public MonitoredProcess getMatchingUProcProcess(String serverProcessId, String serverLaunchId) {
		logStatus("getMatchingUProcProcess");
		for (MonitoredProcess pr: processes) 
			if (serverLaunchId.equals(pr.serverLaunchId) && serverProcessId.equals(pr.serverProcessId))
				return pr;
		return null;
	}
	
	/**
	 * Get a process started by an uproc, the server launch id is not set 
	 * @param serverJobId
	 * @return
	 */
	public MonitoredProcess getPendingServerProcess(String serverJobId) {
		logStatus("getPendingServerProcess");
		for (MonitoredProcess pr: processes) 
			if (serverJobId.equals(pr.serverProcessId) && pr.serverLaunchId == null)
				return pr;
		return null;
	}	
	
	
	/**
	 * Get the process associated to the connection
	 * @param conn
	 * @return
	 */
	public MonitoredProcess getMatchingProcess(Connection conn) {
		logStatus("getMatchingProcess");
		for (MonitoredProcess pr: processes) 
			if (conn.equals(pr.connection))
				return pr;
		return null;
	}
	
	/**
	 * Get a process started by the server, the uproc launch id is not set 
	 * 
	 * @param serverProcessId
	 * @param serverLaunchId
	 * @return
	 */
	public MonitoredProcess getPendingUProcProcess(String serverProcessId, String serverLaunchId) {
		logStatus("getPendingUProcProcess");
		for (MonitoredProcess pr: processes) 
			if (serverLaunchId.equals(pr.serverLaunchId) && serverProcessId.equals(pr.serverProcessId) && pr.clientLaunchId==null)
				return pr;
		return null;
	}

	/**
	 * remove a process from the list
	 * @param mp
	 */
	public void remove(MonitoredProcess mp) {
		logStatus("Removing " + mp.serverProcessId);
		processes.remove(mp);
		logStatus("Removed");
	}
	
	
	public void logStatus(String msg) {
		System.out.println("Process tracker status: " + msg);
		if (processes.size()==0)
			System.out.println("-Empty-");
		for (MonitoredProcess pr : processes) {
			System.out.println("-Server:" + pr.serverProcessId +"/" + pr.serverLaunchId +"\n"
							  +" UProc:" + pr.clientUProcName + "/" + pr.clientLaunchId +"\n"
							  +" Connection:" + (pr.connection!=null));
		}
	}
	
}
