package com.orsyp.integration.engine.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import com.orsyp.integration.engine.messages.FailUprocEvent;
import com.orsyp.integration.engine.messages.LogUprocEvent;
import com.orsyp.integration.engine.messages.RegistrationEvent;
import com.orsyp.integration.engine.messages.StartUprocEvent;
import com.orsyp.integration.engine.messages.StopUprocEvent;

public class ClassRegistration {
	
	public static void register(EndPoint endPoint) {
		Kryo k = endPoint.getKryo();
		k.register(RegistrationEvent.class);
		k.register(LogUprocEvent.class);
		k.register(StopUprocEvent.class);
		k.register(FailUprocEvent.class);
		k.register(StartUprocEvent.class);
	}

}

