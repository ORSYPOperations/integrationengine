package com.orsyp.integration.engine.client;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.orsyp.integration.engine.messages.FailUprocEvent;
import com.orsyp.integration.engine.messages.LogUprocEvent;
import com.orsyp.integration.engine.messages.RegistrationEvent;
import com.orsyp.integration.engine.messages.StartUprocEvent;
import com.orsyp.integration.engine.messages.StopUprocEvent;
import com.orsyp.integration.engine.server.ClassRegistration;
import com.orsyp.integration.engine.util.Log;

public class MonitorClient {

	public static void main(String[] args) {
		int port = 32000;
		
		if (args.length!=5) {
			Log.log("ERROR! Missing startup parameters.\nUse: <uproc> <uprocLaunchId> <serverProcessId> <serverLaunchId> <config_file>");
			int idx=1;
			for (String par : args) {
				System.out.println("PARAM"+idx+": " + par);
				idx++;
			}
			System.exit(1);
		}
		
		String configFile = args[4];
		Properties config = null;
		
		try {
			config = new Properties();
			config.load(new FileInputStream(new File(configFile)));			
		} catch (Exception e) {
			e.printStackTrace();
			Log.log("ERROR reading config file: " + args[4]);
			System.exit(1);
		}			
		
		try {
			port = Integer.parseInt(config.getProperty("port"));
		} catch (Exception e) {	
			Log.log("ERROR configuring client");
			e.printStackTrace();
			System.exit(1);
		}
		
		try {
			new MonitorClient(args[0],args[1],args[2],args[3],port);
		} catch (Exception e) {	
			Log.log("ERROR starting client");
			e.printStackTrace();
			System.exit(1);
		}
			
	}

	
	
	private Client cl;
	private int retCode = -1;
	
	public MonitorClient(final String uproc, final String uprocLaunchId, final String serverJobId, final String serverLaunchId, final int port) throws Exception{
		cl = new Client();
		ClassRegistration.register(cl);
		cl.start();	
        
        cl.addListener(new Listener() {
				            public void connected (Connection connection) {
			                    RegistrationEvent evt = new RegistrationEvent();
			                    evt.clientUProcName = uproc;
			                    evt.clientLaunchId = uprocLaunchId;
			                    evt.serverJobId = serverJobId;
			                    evt.serverLaunchId = serverLaunchId;
			                    evt.message="Register me!";
			                    cl.sendTCP(evt);
				            }
				
				            public void received (Connection connection, Object object) {
				            	if (object instanceof StartUprocEvent) {
				            		StartUprocEvent evt = (StartUprocEvent)object;
			                    	if (evt.message!=null)
			                    		Log.log(evt.message);    
			                        return;
			                    }
				            	else
			                    if (object instanceof LogUprocEvent) {
			                    	LogUprocEvent evt = (LogUprocEvent)object;
			                    	if (evt.message!=null)
			                    		Log.log(evt.message);    
			                        return;
			                    }
				            	else
			                    if (object instanceof StopUprocEvent) {
			                    	StopUprocEvent evt = (StopUprocEvent)object;
			                    	if (evt.message!=null)
			                    		Log.log(evt.message);
			                        cl.close();
			                        retCode=0;
			                        return;
			                    }
			                    else
			                    if (object instanceof FailUprocEvent) {
			                    	FailUprocEvent evt = (FailUprocEvent)object;
			                    	if (evt.message!=null)
			                    		Log.log(evt.message);
			                        cl.close();
			                        retCode=1;
			                        return;
			                    }
				            }
				
				            public void disconnected (Connection connection) {
				            	Log.log("Disconnected from MonitorServer");
				            	if (retCode==-1)
				            		retCode=1;
				            }
				        });
        
        new Thread("Connect") {
            public void run () {
                try {
                	cl.connect(15000, "localhost", port);
                	Log.log("Connected to server");
                } catch (Exception ex) {
                	Log.log("Connection error");
                    ex.printStackTrace();
                    System.exit(1);
                }
            }
        }.start();
        
        try {
	        while (retCode==-1) {
	        	Thread.sleep(200);
	        }
        } catch (Exception ex) {
        	Log.log("Connection error");
            ex.printStackTrace();
            System.exit(retCode);
        }
        
        Log.log("Exiting... retcode="+retCode); 
        System.exit(retCode);
	}

}
