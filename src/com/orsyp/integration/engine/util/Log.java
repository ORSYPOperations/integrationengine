package com.orsyp.integration.engine.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {
	public static void log (String msg) {
		System.out.println(new SimpleDateFormat().format(Calendar.getInstance().getTime()) + " - " +msg);
	}
}
