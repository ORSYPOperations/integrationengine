package com.orsyp.integration.engine.du;

import com.orsyp.api.launch.LaunchId;
import com.orsyp.integration.engine.du.api.DuApiConnection;
import com.orsyp.integration.engine.util.Codec;
import com.orsyp.integration.engine.util.CustomProperties;

public class DuApi implements IDuCommunication {
	
	private String uvmsHost = "localhost";
	private int uvmsPort = 4184;
	private String uvmsUser = "admin";
	private String uvmsPw = "";
	private String uvmsNode = "";
	private String uvmsArea = "EXP";
	private String uvmsApp = "U_";
	
	private DuApiConnection conn;
	
	@Override
	public void configure(CustomProperties config) {
		uvmsHost = config.getProperty("uvmsHost");
		uvmsPort = Integer.parseInt(config.getProperty("uvmsPort"));
		uvmsUser = config.getProperty("uvmsUser");	
		
		uvmsPw = config.getProperty("uvmsPw");
		
		uvmsPw = Codec.decrypt(uvmsPw);
		
		uvmsNode = config.getProperty("uvmsNode");
		uvmsArea = config.getProperty("uvmsArea");
		uvmsApp = config.getProperty("uvmsApp");
		
		conn = new DuApiConnection(uvmsNode, uvmsArea, uvmsHost, uvmsPort, uvmsUser, uvmsPw);
	}	

	@Override
	public void createUProc(String name, String serverJobId, String script) throws Exception {
		conn.createUproc(name, script, "Monitor " + name, uvmsApp, serverJobId);		
	}

	@Override
	public String launchUProc(String name, String serverLaunchId) throws Exception {
		LaunchId id = conn.createLaunch(name,serverLaunchId);
		return id.getNumlanc();
	}


	@Override
	public boolean existsUProc(String name) throws Exception {
		return conn.uprocExists(name);
	}

}
