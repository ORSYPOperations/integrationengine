package com.orsyp.integration.engine.du.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.Product;
import com.orsyp.api.Variable;
import com.orsyp.api.VariableText;
import com.orsyp.api.application.Application;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.launch.Launch;
import com.orsyp.api.launch.LaunchId;
import com.orsyp.api.mu.MuFilter;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.security.Operation;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.uproc.Memorization;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocFilter;
import com.orsyp.api.uproc.UprocId;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.api.uproc.Memorization.Type;
import com.orsyp.api.uproc.cl.InternalScript;
import com.orsyp.api.user.UserFilter;
import com.orsyp.api.user.UserList;
import com.orsyp.central.jpa.jpo.NodeInfoEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.owls.impl.application.OwlsApplicationImpl;
import com.orsyp.owls.impl.launch.OwlsLaunchImpl;
import com.orsyp.owls.impl.mu.OwlsMuListImpl;
import com.orsyp.owls.impl.uproc.OwlsUprocImpl;
import com.orsyp.owls.impl.uproc.OwlsUprocListImpl;
import com.orsyp.owls.impl.user.OwlsUserListImpl;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.MultiCentralConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;

public class DuApiConnection {
	
	private Context context;
	private String defaultVersion = "000";
	private HashMap<String, String> appDomains = new HashMap<String, String>();
	List<String> mus = null; 
	List<String> users = null;
	
	public DuApiConnection(String node, String area, String host, int port, String user, String password) {
		try {
			getNodeConnection(node, area, host, port, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getNodeConnection(String node, String area, String host, int port, String user, String password) throws Exception{
		
		UniCentral central = getUVMSConnection(host, port, user, password);			
		NodeInfoEntity[] nnes = ClientServiceLocator.getNodeInfoService().getAllNodeInfoFromCache(-1, null);
		String company = null;
		
		for (NodeInfoEntity nne : nnes)
			if (nne.getProductCode().equals("DUN"))
				if (nne.getNodeName().equalsIgnoreCase(node)) {
					company = nne.getCompany();
					break;
				}
		
		if (company==null)
			throw new Exception("Node not found");
		
		if (Arrays.asList("A", "APP", "I", "INT").contains(area.toUpperCase())) 
			defaultVersion = "001";
		else
			defaultVersion = "000";
		
		Area a = Area.Exploitation;
		if (Arrays.asList("A", "APP").contains(area.toUpperCase()))
			a = Area.Application;
		else
		if (Arrays.asList("I", "INT").contains(area.toUpperCase()))
			a = Area.Integration;
		else
		if (Arrays.asList("S", "SIM").contains(area.toUpperCase()))
			a = Area.Simulation;
		
		context = makeContext(node, company, central, user, a);
	}
	
	private UniCentral getUVMSConnection(String host, int port, String user, String password) throws SyntaxException {		
		UniCentral cent = new UniCentral(host, port);
		cent.setImplementation(new UniCentralStdImpl(cent));
		
		Context ctx = new Context(new Environment("UJCENT",host), new Client(new Identity(user, password, host, "")));
		ctx.setProduct(com.orsyp.api.Product.UNICENTRAL);
		ctx.setUnijobCentral(cent);
		ClientServiceLocator.setContext(ctx);

		try {
			cent.login(user, password);
			if (ClientConnectionManager.getDefaultFactory() == null) {
	            ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cent;
	}
	
	private Context makeContext(String node, String company, UniCentral central, String user, Area area) throws SyntaxException {
		Context ctx = null;
		Client client = new Client(new Identity(user, "", node, ""));
		ctx = new Context(new Environment(company, node, area), client, central);
		ctx.setProduct(Product.OWLS);
		return ctx;
	}

	
	//-------------------------------------------
	
	
	public boolean uprocExists(String uprocName) throws Exception{			    
    	UprocFilter filter = new UprocFilter();        
        filter.setId("*");
        filter.setName(uprocName);
        filter.setVersion(defaultVersion);
        filter.setLabel("*");
        filter.setApplication("*");
        filter.setDomain("*");
        
        UprocList l = new UprocList(context, filter);
        l.setImpl(new OwlsUprocListImpl());
        l.extract(Operation.DISPLAY);
        
        return l.getCount()==1;
	}
	
	
	public void createUproc(String name, String script, String label, String app, String serverJobId) throws Exception { 
		UprocId uprocId = new UprocId(name, defaultVersion);
		Uproc obj = new Uproc(context, uprocId);
		
		obj.setLabel(label);
		obj.setDefaultSeverity(0);
		obj.setApplication(app);
		obj.setDomain(getDomain(app));
		obj.setType("CL_INT");
		obj.setFunctionalPeriod(FunctionalPeriod.Day);

		obj.setImpl(new OwlsUprocImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.create();

		InternalScript intScript = new InternalScript(obj);
		intScript.setLines(script.split("\\\\n"));
		intScript.save();

		Memorization memo = new Memorization();
		memo.setNumber(1); 
		memo.setType(Type.ONE);			
				
		obj.setMemorization(memo);
		
		// add uproc variables
		Vector<Variable> varVector = new Vector<Variable>();
				
		VariableText varText = new VariableText();
		varText.setName("MONSRV_JOB_ID");
		varText.setValue(serverJobId);
		varText.setLength(255);
		varText.setOrigin(Variable.ORIGIN_UPROC);
		varVector.add(varText);
		
		varText = new VariableText();
		varText.setName("MONSRV_LAUNCH_ID");
		varText.setValue("?");
		varText.setLength(255);
		varText.setOrigin(Variable.ORIGIN_UPROC);
		varVector.add(varText);

		obj.setVariables(varVector);
		
		obj.update();
	}	
	
	public LaunchId createLaunch(String uprocName, String serverLaunchId) throws Exception {
		if (mus==null)
			mus = getMus(); 
		if (users==null) 
			users = getUsers();
		if (mus.size()==0)
			throw new Exception("No MU found");
		if (users.size()==0)
			throw new Exception("No user found");
		String aMu = mus.get(0);
		String aUser = users.get(0);
		if (users.contains("univ60a"))
			aUser = "univ60a";		
		
		Launch l = new Launch(context,LaunchId.createWithName("", "", uprocName, "000", aMu, null));		
		l.setImpl(new OwlsLaunchImpl());
		l.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		
		Date now = Calendar.getInstance().getTime();
        l.setBasedOnTask(false);
        l.setBeginDate(now);
        Date endDate = new Date();
        endDate.setTime(now.getTime() + 100000000);
        l.setEndDate(endDate);
        l.setProcessingDate((new SimpleDateFormat("yyyyMMdd")).format(now));
        l.setUserName(aUser);
        l.setQueue("SYS_BATCH");
        l.setPriority("001");
        l.setPrinter("prin");
        
        
        //add launch variable
     	Variable[] vars = new Variable[1];
     		
     	VariableText var = new VariableText();
     	var.setName("MONSRV_LAUNCH_ID");
     	var.setValue(serverLaunchId);
     	var.setLength(255);
     	var.setOrigin(Variable.ORIGIN_LAUNCH);
     	
     	vars[0] = var;

     	l.setVariables(vars);
        
        l.create();		
        return l.getIdentifier();
	}
	
	private List<String> getMus() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		MuList l = new MuList(context, new MuFilter());
    	l.setImpl(new OwlsMuListImpl());
		l.extract();
		
		for (int i = 0; i < l.getCount(); i++)
			list.add(l.get(i).getName());		
		return list;
	}
	
	private List<String> getUsers() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		UserList l = new UserList(context, new UserFilter());
    	l.setImpl(new OwlsUserListImpl());
		l.extract();
		
		for (int i = 0; i < l.getCount(); i++)
			list.add(l.get(i).getName());		
		return list;
	}
	
	//-----------------------------------------
	
	private Application getApplication(String appl) throws Exception {
		Application app = new Application(context, appl); 
		app.setImpl(new OwlsApplicationImpl());
        app.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
        app.extract();
        return app;
	}
	
	private String getDomain(String applicationId) throws Exception {
		String domain = appDomains.get(applicationId);
		if (domain==null) {
			Application app = getApplication(applicationId);
			domain = app.getDomain();
			appDomains.put(applicationId, domain);
		}			
		return domain;
	}
}
