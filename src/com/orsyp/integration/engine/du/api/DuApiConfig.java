package com.orsyp.integration.engine.du.api;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.orsyp.integration.engine.util.Codec;
import com.orsyp.integration.engine.util.CustomProperties;

public class DuApiConfig {
	
	public static void main(String[] args) {
		String configFile = "monitor.conf";
		if (args.length>0)
			configFile = args[0];
		File f = new File(configFile);
		Scanner s = new Scanner(System.in);
		if (f.exists()) {
			CustomProperties prop = new CustomProperties();
			prop.load(configFile);			
			String user =prop.getProperty("uvmsUser");
			System.out.println("Insert password for uvms user " + user);
			String pw = s.nextLine();
			try {
				PrintWriter output = new PrintWriter(new FileWriter(configFile, true));
				output.println("");
				output.println("");
				output.println("# encrypted uvms password");
				output.println("uvmsPw="+Codec.encrypt(pw));
				output.close();
				System.out.println("Password configured correctly");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error storing encrypted password");
			}
		}
		else
			System.out.println("ERROR: can't find configuration file '"+configFile+"'");
		System.out.println("");
		System.out.println("Press RETURN to continue...");
		s.nextLine();
		s.close();
	}
}
