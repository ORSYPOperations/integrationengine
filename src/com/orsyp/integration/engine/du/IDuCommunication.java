package com.orsyp.integration.engine.du;

import com.orsyp.integration.engine.util.CustomProperties;

public interface IDuCommunication {
	
	public void createUProc(String name, String serverJobId, String script) throws Exception;
	
	public String launchUProc(String name, String serverLaunchId) throws Exception;
	
	public boolean existsUProc(String name) throws Exception;

	public void configure(CustomProperties config); 

}
