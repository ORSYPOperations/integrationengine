package com.orsyp.integration.engine.du;

import com.orsyp.integration.engine.util.CustomProperties;

public class DuScript implements IDuCommunication {
	
	private boolean unix = true;
	private String duPath;


	@Override
	public boolean existsUProc(String name) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void configure(CustomProperties config) {
		if (config.getProperty("os")!=null)
			unix = config.getProperty("os").equalsIgnoreCase("unix");
		duPath = config.getProperty("$U_path");
	}

	@Override
	public void createUProc(String name, String serverJobId, String script)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String launchUProc(String name, String serverLaunchId)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
