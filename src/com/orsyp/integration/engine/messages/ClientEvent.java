package com.orsyp.integration.engine.messages;

import java.io.Serializable;

public class ClientEvent implements Serializable {
	private static final long serialVersionUID = 1L;
		
	public String uproc;
	public String launchId;
	public String serverId;

}
