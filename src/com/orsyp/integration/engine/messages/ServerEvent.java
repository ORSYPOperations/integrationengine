package com.orsyp.integration.engine.messages;

import java.io.Serializable;

public class ServerEvent implements Serializable {
	private static final long serialVersionUID = 1L;
		
	//target system job name
	public String serverJobId;
	//target system job launch id
	public String serverLaunchId;
	
	//uproc name
	public String clientUProcName;
	//launch id
	public String clientLaunchId;
		
	public String message;

}
